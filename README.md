## **Sarcasm in Text?**
==================

Back when texting and instant messaging were first introduced, tone of voice and mood were difficult to distinguish.  Then came about the use of keyboard symbols to create emotion in texts, such as the following:

- :)
- :/
- :(

Those are still used today but not near as much, nor do the have the same meanings in certain contexts, and are sometimes used ironically.

Today, we have what are called Emojis, or for a more universal term, emoticons.  There are a total of 2,823 emojis in use now, making it easier to convey what you are feeling with one single click on your phone keyboard.  :iphone:
Here are the emoji equivalents of the symbols I used earlier:

- :blush:
- :confused:
- :disappointed:

There is sometimes still confusion over the intention of text messages, but it has been made a hundred times easier when you have an emoji for almost ***any*** situation.  

There is also a new technology Apple debuted in 2017 with the launch of the iPhone X, the Animoji and Memoji.  Animoji gives you the option to choose an animal and then put your face in the screen square and press record. What you say and do with your face will be depicted on the animal video.  Then later, the [Memoji](https://www.youtube.com/watch?v=Qgy6LaO3SB0) comes about, which is the same thing, but using facial recognition to create an animated emoji of your face.  

![Imgur](https://i.imgur.com/ivVltww.png)

Above, you can see a little insight into how technology has evolved into being able to easily display emotion and tone of voice, instead of being commonly misconstrued like in the past.  

Lauryn Pedtke